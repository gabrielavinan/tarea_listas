#gabriela.vinan@unl.edu.ec
# Read an integer:
# a = int(input())
# Print a value:
# print(a)
n = int(input())
a = [[abs(i - j) for j in range(n)] for i in range(n)]
for row in a:
  print(' '.join([str(i) for i in row]))
