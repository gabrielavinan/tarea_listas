#gabriela.vinan@unl.edu.ec
# Read a list of integers:
lst = list([int(s) for s in input().split()])
max, min = lst.index(max(lst)), lst.index(min(lst))
lst[max], lst[min] = lst[min], lst[max]
print(lst)